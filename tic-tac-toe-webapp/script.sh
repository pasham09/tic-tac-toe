#!/bin/bash
# Checks if eureka server has discovered user microservice.

read_dom() {
  local IFS=\>
  read -d \< ENTITY CONTENT
}

STATUS=DOWN

URL=http://localhost:8761/eureka/apps/usermicroservice
output_xml=usermicroservice.xml

until [[ $STATUS = "UP" ]]; do

  curl "$URL?$(date +%s)" -o $output_xml

  while read_dom; do
    if [[ $ENTITY = "status" ]]; then
      STATUS=$CONTENT
      rm $output_xml
    fi
  done < $output_xml
    
  sleep 5

done

exec "$@"
