package com.tictactoe.mappers;

import com.tictactoe.dtos.UserDTO;
import com.tictactoe.models.User;

public class UserMapper {
  public static UserDTO map(User user) {
    UserDTO userDTO = new UserDTO();
    userDTO.setActive(user.getActive());
    userDTO.setUserName(user.getUserName());
    userDTO.setRoles(user.getRoles());
    userDTO.setLosesCount(user.getLosesCount());
    userDTO.setWinsCount(user.getWinsCount());

    return userDTO;
  }
}
