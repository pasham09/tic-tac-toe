package com.tictactoe.mappers;

import com.tictactoe.dtos.UserCreationDTO;
import com.tictactoe.models.User;

import org.springframework.security.crypto.password.PasswordEncoder;

public class UserCreationMapper {

  public static User map(UserCreationDTO userCreationDTO, PasswordEncoder passwordEncoder) {
    User user = new User();
    user.setActive(userCreationDTO.getActive()); 
    user.setUserName(userCreationDTO.getUserName());
    user.setPassword(passwordEncoder.encode(userCreationDTO.getPassword()));
    user.setRoles(userCreationDTO.getRoles());
    user.setWinsCount(userCreationDTO.getWinsCount());
    user.setLosesCount(userCreationDTO.getLosesCount());

    return user;
  }  
}
