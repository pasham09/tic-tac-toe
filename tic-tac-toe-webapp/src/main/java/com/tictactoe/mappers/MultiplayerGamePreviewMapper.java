package com.tictactoe.mappers;

import com.tictactoe.dtos.MultiplayerGamePreviewDTO;
import com.tictactoe.models.MultiplayerGame;
import com.tictactoe.models.PersonPlayer;

public class MultiplayerGamePreviewMapper {
  public static MultiplayerGamePreviewDTO map(MultiplayerGame multiplayerGame) {
    MultiplayerGamePreviewDTO multiplayerGamePreviewDTO = new MultiplayerGamePreviewDTO();
    multiplayerGamePreviewDTO.setId(multiplayerGame.getId());

    PersonPlayer firstPlayer = multiplayerGame.getFirstPlayer();
    PersonPlayer secondPlayer = multiplayerGame.getSecondPlayer(); 

    if (firstPlayer != null) {
      multiplayerGamePreviewDTO.setFirstPlayerUserName(firstPlayer.getUsername());
    }

    if (secondPlayer != null) {
      multiplayerGamePreviewDTO.setSecondPlayerUserName(secondPlayer.getUsername());
    }

    return multiplayerGamePreviewDTO;
  } 
}
