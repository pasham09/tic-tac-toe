package com.tictactoe.actuator;

import com.tictactoe.repos.QBotRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class QBotsHealthIndicator implements HealthIndicator {

  @Autowired
  private QBotRepository qBotRepository;

  @Override
  public Health health() {
    boolean hasPassedCheck = check();
    if (!hasPassedCheck) {
      return Health.down().withDetail("QBots repository", "Empty").build();
    }
    return Health.up().withDetail("QBots repository", "Has bots").build();
  }

  private boolean check() {
    return qBotRepository.count() > 0 ? true : false;
  }
}
