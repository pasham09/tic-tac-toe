package com.tictactoe.utils;

import java.util.Arrays;

import com.tictactoe.models.MultiplayerGame;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

  private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

  @AfterReturning(value = "execution(* org.springframework.security.authentication.AuthenticationManager.authenticate(..))", returning = "result")
  public void trackSuccessfulLogin(JoinPoint joinPoint, Object result) {
    logger.info("User with username: " + ((Authentication) result).getName() + " has successfully logged in.");
  }

  @AfterReturning(value = "execution(public Integer com.tictactoe.controllers.QBotController.playPosition(..)) && args(symbol, board)", returning = "action")
  public void trackAiGame(JoinPoint joinPoint, Integer symbol, int[] board, Integer action) {
    int[] copiedBoard = Arrays.copyOf(board, board.length);

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String currentPrincipalName = authentication.getName();

    logger.info("Person with username: " + currentPrincipalName + " made move with symbol: " + -symbol);
    logger.info("Position now is: " + Arrays.toString(copiedBoard));

    copiedBoard[action] = symbol;

    logger.info("AI responded with move, using symbol: " + symbol);
    logger.info("Position now is: " + Arrays.toString(copiedBoard));
  }

  @Before("execution(void com.tictactoe.repos.MultiplayerGameRepository.updateFirstPlayer(..)) && args(Id, userName, ..)")
  public void trackFirstPlayerJoinGame(JoinPoint joinPoint, Integer Id, String userName) {
    logger.info("First player with username: " + userName + " joined game. Game id is: " + Id);
  }

  @Before("execution(void com.tictactoe.repos.MultiplayerGameRepository.updateSecondPlayer(..)) && args(Id, userName, ..)")
  public void trackSecondPlayerJoinGame(JoinPoint joinPoint, Integer Id, String userName) {
    logger.info("Second player with username: " + userName + " joined game. Game id is: " + Id);
  }

  @Before("execution(public void com.tictactoe.services.MultiplayerGameService.makeAction(..)) && args(multiplayerGame, action, symbolForPlayer)")
  public void trackPlayerAction(JoinPoint joinPoint, MultiplayerGame multiplayerGame, int action, int symbolForPlayer) {
    String strSymbol = symbolForPlayer == 1 ? "X" : "O";
    logger.info("Player with the symbol: " + strSymbol + " made move on cell: " + action + ". Game id is: "
        + multiplayerGame.getId());
  }
}
