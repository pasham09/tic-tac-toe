package com.tictactoe.utils;

import com.tictactoe.models.Player;

public class GameResult {
  private Player winner;
  private Player loser;
  private boolean isDraw;

  public void setWinner(Player winner) {
    this.winner = winner;
  }

  public Player getWinner() {
    return winner;
  }

  public void setLoser(Player loser) {
    this.loser = loser;
  }

  public Player getLoser() {
    return loser;
  }

  public void setDraw() {
    isDraw = true;
  }

  public boolean isDraw() {
    return isDraw;
  }
}
