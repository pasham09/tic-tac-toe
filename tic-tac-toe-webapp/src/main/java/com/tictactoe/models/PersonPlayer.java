package com.tictactoe.models;

public class PersonPlayer extends Player {
  private String username; 

  public PersonPlayer() {}
  
  public PersonPlayer(String userName) {
    this.username = userName;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }
}
