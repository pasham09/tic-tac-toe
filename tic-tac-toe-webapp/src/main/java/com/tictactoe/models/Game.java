package com.tictactoe.models;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;

import com.tictactoe.utils.Board;

@MappedSuperclass
public class Game<T extends Player> {
  public static final int BOARD_ROWS = 3;
  public static final int BOARD_COLUMNS = 3; 

  protected int playerToMoveSymbol;

  @Embedded
  protected Board board;

  @Embedded
  @AttributeOverrides({
    @AttributeOverride(name = "symbol", column = @Column(name = "firstPlayerSymbol")),
    @AttributeOverride(name = "username", column = @Column(name = "firstPlayerUserName"))
  })
  protected T firstPlayer;

  @Embedded
  @AttributeOverrides({
    @AttributeOverride(name = "symbol", column = @Column(name = "secondPlayerSymbol")),
    @AttributeOverride(name = "username", column = @Column(name = "secondPlayerUserName"))
  })
  protected T secondPlayer;

  public int getPlayerToMoveSymbol() {
    return playerToMoveSymbol;
  }

  public void setPlayerToMoveSymbol(int playerToMoveSymbol) {
    this.playerToMoveSymbol = playerToMoveSymbol;
  }

  public Board getBoard() {
    return board;
  } 

  public void setBoard(Board board) {
    this.board = board;
  }

  public T getFirstPlayer() {
    return firstPlayer;
  }

  public void setFirstPlayer(T firstPlayer) {
    this.firstPlayer = firstPlayer;
  }

  public T getSecondPlayer() {
    return secondPlayer;
  }

  public void setSecondPlayer(T secondPlayer) {
    this.secondPlayer = secondPlayer;
  }
}
