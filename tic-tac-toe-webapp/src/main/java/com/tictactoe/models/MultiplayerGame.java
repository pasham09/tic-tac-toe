package com.tictactoe.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.tictactoe.qlearning.QBotGame;
import com.tictactoe.utils.Board;

@Entity
@Table(name = "game")
public class MultiplayerGame extends Game<PersonPlayer> {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  public MultiplayerGame() {
    playerToMoveSymbol = 1;
    board = new Board(QBotGame.BOARD_ROWS, QBotGame.BOARD_COLUMNS);
  }

  public Integer getId() {
    return id;
  }

}
