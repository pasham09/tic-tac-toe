package com.tictactoe.models;

import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Embeddable
public class Player {
  
  protected int symbol;

  public int getSymbol() {
    return symbol;
  }

  public void setSymbol(int symbol) {
    this.symbol = symbol;
  }
}
