package com.tictactoe.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "bots")
public class QBot extends Player {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  @ElementCollection
  @CollectionTable(name = "states_value", joinColumns = {@JoinColumn(name = "bot_id", referencedColumnName = "id")})
  @MapKeyColumn(name = "state_hash")
  @Column(name = "state_value")
  private Map<Integer, Double> statesValue;

  private double lr = 0.2;
  private double decayGamma = 0.9;
  private double expRate = 0.3;

  @Transient
  private List<Integer> states;

  public QBot() {
    this(1);
  }

  public QBot(int symbol) {
    this.symbol = symbol;
    states = new ArrayList<Integer>();
    statesValue = new HashMap<Integer, Double>();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Map<Integer, Double> getStatesValue() {
    return statesValue;
  }

  public void setStatesValue(Map<Integer, Double> statesValue) {
    this.statesValue = statesValue;
  }

  public List<Integer> getStates() {
    return states;
  }

  public void setStates(List<Integer> states) {
    this.states = states;
  }

  public double getExpRate() {
    return expRate;
  }
  
  public void setExpRate(double expRate) {
    this.expRate = expRate;
  }

  public double getLr() {
    return lr;
  }

  public void setLr(double lr) {
    this.lr = lr;
  }

  public double getDecayGamma() {
    return decayGamma;
  }

  public void setDecayGamma(double decayGamma) {
    this.decayGamma = decayGamma;
  }
}
