package com.tictactoe.qlearning;

import com.tictactoe.models.Game;
import com.tictactoe.models.QBot;
import com.tictactoe.services.QBotInGameService;
import com.tictactoe.utils.Board;

public class QBotGame extends Game<QBot> {

  private QBotInGameService qBotInGameService = new QBotInGameService();

  public QBotGame(QBot firstBot, QBot secondBot) {
    playerToMoveSymbol = 1;
    board = new Board(QBotGame.BOARD_ROWS, QBotGame.BOARD_COLUMNS);

    if (firstBot.getSymbol() == 1 && secondBot.getSymbol() == -1) {
      this.firstPlayer = firstBot;
      this.secondPlayer = secondBot;
    } else {
      this.firstPlayer = secondBot;
      this.secondPlayer = firstBot;
    }
  }

  /**
   * Chooses bot to move next and makes next move for it.
   */
  public void makeAction() {
    QBot botToMove = (QBot) (playerToMoveSymbol == 1 ? firstPlayer : secondPlayer);

    int botAction = qBotInGameService.chooseAction(botToMove, board);

    board.setCell(botAction, playerToMoveSymbol);
    playerToMoveSymbol = -playerToMoveSymbol; 

    qBotInGameService.addState(botToMove, board.hashCode());
  }
 
}