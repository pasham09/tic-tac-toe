package com.tictactoe.qlearning;

import com.tictactoe.models.QBot;
import com.tictactoe.services.GameService;
import com.tictactoe.services.QBotInGameService;
import com.tictactoe.utils.GameResult;

public class QLearner {

  private QBotInGameService qBotInGameService = new QBotInGameService();
  private GameService gameService = new GameService();

  /**
   * Trains QBot with another QBot for specified number of games.
   * @param bot Bot to train.
   * @param rounds Amount of games bot will play.
   */
  public void trainQBot(QBot bot, int rounds) {
    QBot botToTrainWith = new QBot(-bot.getSymbol());

    for (int i = 0; i < rounds; i++) {
      QBotGame game = new QBotGame(bot, botToTrainWith);
      qBotInGameService.reset(bot);
      qBotInGameService.reset(botToTrainWith);

      while (true) {
        game.makeAction();
        GameResult gameResult = gameService.getResult(game);

        if (gameResult == null)
          continue;

        if (gameResult.isDraw()) {
          qBotInGameService.feedReward(bot, 0.3);
          qBotInGameService.feedReward(botToTrainWith, 0.3);
          break;
        } else {
          qBotInGameService.feedReward((QBot) gameResult.getWinner(), 1);
          qBotInGameService.feedReward((QBot) gameResult.getLoser(), 0);
          break;
        }
      }
    }
  }
  
}