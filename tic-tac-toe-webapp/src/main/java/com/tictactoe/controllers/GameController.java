package com.tictactoe.controllers;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.tictactoe.dtos.MultiplayerGamePreviewDTO;
import com.tictactoe.mappers.MultiplayerGamePreviewMapper;
import com.tictactoe.models.MultiplayerGame;
import com.tictactoe.models.PersonPlayer;
import com.tictactoe.services.MultiplayerGameService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("game")
public class GameController {
  @Autowired
  private MultiplayerGameService multiplayerGameService;

  @PostMapping("create")
  public void createGame(Principal principal) {
    MultiplayerGame multiplayerGame = new MultiplayerGame();
    multiplayerGameService.save(multiplayerGame);
  }

  @GetMapping("all")
  public List<MultiplayerGamePreviewDTO> gamesList() {
    return StreamSupport.stream(multiplayerGameService.findAll().spliterator(), false)
      .map(MultiplayerGamePreviewMapper::map)
      .collect(Collectors.toList());
  }

  @PostMapping("join/{gameId}")
  public void joinGame(@PathVariable Integer gameId, Principal principal) {
    PersonPlayer personPlayer = new PersonPlayer(principal.getName());
    MultiplayerGame multiplayerGame = multiplayerGameService.findById(gameId);

    if (multiplayerGame == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Multiplayer game is not found.");

    if (!multiplayerGameService.hasPlayer(multiplayerGame, personPlayer.getUsername())) {
      multiplayerGameService.addPlayer(multiplayerGame, personPlayer);
    }
  }

  @GetMapping("symbol/{gameId}")
  public int getSymbol(@PathVariable Integer gameId, Principal principal) {
    MultiplayerGame multiplayerGame = multiplayerGameService.findById(gameId);
    if (multiplayerGame == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Multiplayer game is not found.");

    return multiplayerGameService.findPlayerSymbol(multiplayerGame, principal.getName());
  }

  @GetMapping("/{gameId}/board")
  public int[] getBoard(@PathVariable Integer gameId) {
    MultiplayerGame multiplayerGame = multiplayerGameService.findById(gameId);
    if (multiplayerGame == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Multiplayer game is not found.");

    return multiplayerGame.getBoard().getBoardArray();
  }

  @MessageMapping("/{gameId}/action")
  @SendTo("/topic/game/{gameId}")
  public int[] makeAction(@DestinationVariable Integer gameId, int action, Principal principal) {
    MultiplayerGame multiplayerGame = multiplayerGameService.findById(gameId);
    if (multiplayerGame == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Multiplayer game is not found.");
      
    String userName = principal.getName();

    if (multiplayerGameService.hasPlayer(multiplayerGame, userName)) {
      int symbolForPlayer = multiplayerGameService.getSymbolForPlayer(multiplayerGame, userName);

      if (symbolForPlayer == multiplayerGame.getPlayerToMoveSymbol())
        multiplayerGameService.makeAction(multiplayerGame, action, symbolForPlayer);
    }
    return multiplayerGame.getBoard().getBoardArray();
  }

}
