package com.tictactoe.controllers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.tictactoe.dtos.UserDTO;
import com.tictactoe.mappers.UserMapper;
import com.tictactoe.models.User;
import com.tictactoe.services.WebUsersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("admin")
public class AdminController {

  @Autowired
  private WebUsersService webUsersService;

  @GetMapping("panel")
  public String adminPanel(Model model) {
    Iterable<User> users = webUsersService.findAll();
    List<UserDTO> usersDto = StreamSupport.stream(users.spliterator(), false).map(UserMapper::map)
        .collect(Collectors.toList());

    model.addAttribute("users", usersDto);
    return "adminpanel";
  }

  @GetMapping("user/edit/{userName}")
  public String editUserForm(@PathVariable String userName, Model model) {
    User user = webUsersService.findByUserName(userName);
    if (user == null)
      return "useredit?error";

    UserDTO userDTO = UserMapper.map(user);
    model.addAttribute("user", userDTO);
    return "useredit";
  }

  @PostMapping("user/edit/{userName}")
  public String editUserSubmit(@PathVariable String userName, @RequestParam String updatedUserName,
      @RequestParam(defaultValue = "false") boolean adminRole, @RequestParam(defaultValue = "false") boolean userRole) {

    String roles = "";
    if (adminRole)
      roles += "ROLE_ADMIN,";

    if (userRole)
      roles += "ROLE_USER";

    webUsersService.editUser(userName, updatedUserName, roles);

    return "redirect:/admin/panel";
  }

}
