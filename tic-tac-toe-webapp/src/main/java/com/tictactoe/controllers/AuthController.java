package com.tictactoe.controllers;

import com.tictactoe.dtos.UserCreationDTO;
import com.tictactoe.mappers.UserCreationMapper;
import com.tictactoe.models.User;
import com.tictactoe.services.WebUsersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AuthController {
  @Autowired
  WebUsersService webUsersService;

  @Autowired
  PasswordEncoder passwordEncoder;

  @GetMapping("login")
  public String login() {
    return "login";
  }

  @GetMapping("register")
  public String registerForm() {
    return "register";
  }

  @PostMapping("register")
  public String registerSubmit(@ModelAttribute UserCreationDTO userCreationDTO) {
    User user = UserCreationMapper.map(userCreationDTO, passwordEncoder);
    HttpStatus httpStatus = webUsersService.save(user);

    if (httpStatus != HttpStatus.OK)
      return "redirect:/register?error";

    return "redirect:/login";
  }
}
