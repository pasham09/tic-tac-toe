package com.tictactoe.controllers;

import java.security.Principal;

import com.tictactoe.models.User;
import com.tictactoe.services.WebUsersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("user")
public class UserController {

  @Autowired
  private WebUsersService webUsersService;

  @GetMapping("profile")
  public String profile(Principal principal, Model model) {
    User user = webUsersService.findByUserName(principal.getName());
    if (user == null)
      return "profile?error";

    model.addAttribute("wins", user.getWinsCount());
    model.addAttribute("loses", user.getLosesCount());
    return "profile";
  }

  @PostMapping("updatewins")
  @ResponseStatus(value = HttpStatus.OK)
  public void updateWins(Principal principal) {
    String userName = principal.getName();
    webUsersService.incrementWins(userName);
  }

  @PostMapping("updateloses")
  @ResponseStatus(value = HttpStatus.OK)
  public void updateLoses(Principal principal) {
    String userName = principal.getName();
    webUsersService.incrementLoses(userName);
  }
}
