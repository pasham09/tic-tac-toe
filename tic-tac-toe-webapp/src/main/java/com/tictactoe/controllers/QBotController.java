package com.tictactoe.controllers;

import com.tictactoe.models.QBot;
import com.tictactoe.qlearning.QLearner;
import com.tictactoe.services.QBotService;
import com.tictactoe.services.QBotInGameService;
import com.tictactoe.utils.Board;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("bot")
public class QBotController {

  @Autowired
  private QBotService qBotService;

  @PostMapping("add")
  @ResponseStatus(value = HttpStatus.OK)
  public void addNewBot(@RequestBody int botSymbol) {
    if (Math.abs(botSymbol) != 1)
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Symbol can only be 1(X) or -1(O).");

    QBot qBot = new QBot(botSymbol);
    QLearner qLearner = new QLearner();
    qLearner.trainQBot(qBot, 500);

    qBotService.save(qBot);
  }

  @PostMapping("play/{symbol}")
  public Integer playPosition(@PathVariable Integer symbol, @RequestBody int[] board) {
    QBot qBot = qBotService.findFirstBySymbol(symbol);
    if (qBot == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "QBot is not found.");

    Board newBoard = new Board(board);
    QBotInGameService qBotInGameService = new QBotInGameService();
    int action = qBotInGameService.chooseActionInGame(qBot, newBoard);

    return action;
  }
}