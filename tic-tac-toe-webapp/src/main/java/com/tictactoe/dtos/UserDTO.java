package com.tictactoe.dtos;

public class UserDTO {
  private String userName;
  private boolean active;
  private String roles;
  private int winsCount;
  private int losesCount;

  public int getWinsCount() {
    return winsCount;
  }

  public void setWinsCount(int winsCount) {
    this.winsCount = winsCount;
  }

  public int getLosesCount() {
    return losesCount;
  } 

  public void setLosesCount(int losesCount) {
    this.losesCount = losesCount;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public boolean getActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public String getRoles() {
    return roles;
  }

  public void setRoles(String roles) {
    this.roles = roles;
  }
}
