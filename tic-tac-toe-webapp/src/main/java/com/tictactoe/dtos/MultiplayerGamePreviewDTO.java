package com.tictactoe.dtos;

public class MultiplayerGamePreviewDTO {
  private Integer id;
  private String firstPlayerUserName;
  private String secondPlayerUserName;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFirstPlayerUserName() {
    return firstPlayerUserName;
  }

  public void setFirstPlayerUserName(String firstPlayerUserName) {
    this.firstPlayerUserName = firstPlayerUserName;
  }

  public String getSecondPlayerUserName() {
    return secondPlayerUserName;
  }

  public void setSecondPlayerUserName(String secondPlayerUserName) {
    this.secondPlayerUserName = secondPlayerUserName;
  }

  public String toString() {
    return firstPlayerUserName;
  }
}
