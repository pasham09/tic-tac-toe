package com.tictactoe.repos;

import java.util.Optional;

import com.tictactoe.models.QBot;

import org.springframework.data.repository.CrudRepository;

public interface QBotRepository extends CrudRepository<QBot, Integer> {
  Optional<QBot> findFirstBySymbol(int symbol);
}
