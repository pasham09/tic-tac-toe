package com.tictactoe.repos;

import com.tictactoe.models.MultiplayerGame;
import com.tictactoe.utils.Board;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface MultiplayerGameRepository extends CrudRepository<MultiplayerGame, Integer> {

  @Modifying
  @Transactional
  @Query(value = "update game g set g.first_player_user_name = :userName, g.first_player_symbol = :symbol where g.id = :id", nativeQuery = true)
  void updateFirstPlayer(@Param(value = "id") Integer Id, @Param(value = "userName") String userName, @Param(value = "symbol") int symbol);
  
  @Modifying
  @Transactional
  @Query(value = "update game g set g.second_player_user_name = :userName, g.second_player_symbol = :symbol where g.id = :id", nativeQuery = true)
  void updateSecondPlayer(@Param(value = "id") Integer Id, @Param(value = "userName") String userName, @Param(value = "symbol") int symbol);

  @Modifying
  @Transactional
  @Query("update MultiplayerGame g set g.board = :board, g.playerToMoveSymbol = -g.playerToMoveSymbol where g.id = :id")
  void updateBoard(@Param(value = "id") Integer Id, @Param(value = "board") Board board);
}