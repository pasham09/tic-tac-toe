package com.tictactoe.services;

import com.tictactoe.models.MyUserDetails;
import com.tictactoe.models.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

  @Autowired
  private WebUsersService webUsersService;

  @Override
  public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
    User user = webUsersService.findByUserName(userName);
    if (user == null)
      throw new UsernameNotFoundException("Not found " + userName);
    return new MyUserDetails(user);
  }

}
