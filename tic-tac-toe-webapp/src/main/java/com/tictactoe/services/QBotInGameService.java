package com.tictactoe.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tictactoe.models.QBot;
import com.tictactoe.utils.Board;

public class QBotInGameService {
  /**
   * Choose the best action for the given position, or with the chance of expRate
   * choose a random position.
   * 
   * @param curBoard Position on the board.
   * @return Integer that represents cell on the board.
   */
  public int chooseAction(QBot qBot, Board curBoard) {
    int action = 0;
    List<Integer> availablePositions = curBoard.getAvailablePositions();

    if (Math.random() <= qBot.getExpRate()) {
      int idx = (int) (Math.random() * availablePositions.size());
      action = availablePositions.get(idx);
    } else {
      double valueMax = Double.NEGATIVE_INFINITY;

      for (Integer p : availablePositions) {
        Board nextBoard = new Board(curBoard.getBoardArray());
        nextBoard.setCell(p, qBot.getSymbol());
        int nextBoardHash = nextBoard.hashCode();

        Map<Integer, Double> statesValue = qBot.getStatesValue();
        double value = statesValue.get(nextBoardHash) == null ? 0 : statesValue.get(nextBoardHash);
        if (value > valueMax) {
          valueMax = value;
          action = p;
        }
      }
    }

    return action;
  }

  public int chooseActionInGame(QBot qBot, Board curBoard) {
    List<Integer> availablePositions = curBoard.getAvailablePositions();

    List<Integer> bestActions = new ArrayList<>();
    List<Double> bestValues = new ArrayList<>();
    for (int i = 0; i < availablePositions.size() && i < 3; i++) {
      bestActions.add(availablePositions.get(i));
      bestValues.add(Double.NEGATIVE_INFINITY);
    }

    for (Integer p : availablePositions) {
      Board nextBoard = new Board(curBoard.getBoardArray());
      nextBoard.setCell(p, qBot.getSymbol());
      int nextBoardHash = nextBoard.hashCode();

      Map<Integer, Double> statesValue = qBot.getStatesValue();
      double value = statesValue.get(nextBoardHash) == null ? 0 : statesValue.get(nextBoardHash);

      double min = Double.POSITIVE_INFINITY;
      int minIdx = 0;
      for (int i = 0; i < bestValues.size(); i++) {
        if (bestValues.get(i) < min) {
          min = bestValues.get(i);
          minIdx = i;
        }
      }

      if (value > min) {
        bestValues.set(minIdx, value);
        bestActions.set(minIdx, p);
      }
    }

    double max = Double.NEGATIVE_INFINITY;
    int maxIdx = 0;
    for (int i = 0; i < bestValues.size(); i++) {
      if (bestValues.get(i) > max) {
        max = bestValues.get(i);
        maxIdx = i;
      }
    }

    int idx = (int) (Math.random() * bestActions.size());

    if ((bestValues.get(maxIdx) - bestValues.get(idx)) > 0.1) {
      return bestActions.get(maxIdx);
    } else {
      return bestActions.get(idx);
    }
  }

  /**
   * Updates values of statesValue map based on the reward.
   * 
   * @param reward Number that represent award for the result of the game.
   */
  public void feedReward(QBot qBot, double reward) {
    List<Integer> states = qBot.getStates();
    Map<Integer, Double> statesValue = qBot.getStatesValue();

    for (int i = states.size() - 1; i > -1; i--) {
      int st = states.get(i);
      if (statesValue.get(st) == null)
        statesValue.put(st, 0.0);

      statesValue.put(st, qBot.getLr() * (qBot.getDecayGamma() * reward - statesValue.get(st)));
      reward = statesValue.get(st);
    }
  }

  public void reset(QBot qBot) {
    qBot.setStates(new ArrayList<Integer>());
  }

  public void addState(QBot qBot, int state) {
    qBot.getStates().add(state);
  }
}
