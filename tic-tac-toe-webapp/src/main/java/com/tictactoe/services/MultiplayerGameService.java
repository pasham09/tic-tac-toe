package com.tictactoe.services;

import java.util.Optional;

import com.tictactoe.models.MultiplayerGame;
import com.tictactoe.models.PersonPlayer;
import com.tictactoe.repos.MultiplayerGameRepository;
import com.tictactoe.utils.Board;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MultiplayerGameService {
  @Autowired
  MultiplayerGameRepository multiplayerGameRepository;

  public void save(MultiplayerGame multiplayerGame) {
    multiplayerGameRepository.save(multiplayerGame);
  }

  public Iterable<MultiplayerGame> findAll() {
    return multiplayerGameRepository.findAll();
  }

  public MultiplayerGame findById(Integer Id) {
    Optional<MultiplayerGame> multiplayerGame = multiplayerGameRepository.findById(Id);
    if (!multiplayerGame.isPresent())
      return null;

    return multiplayerGame.get();
  }

  public boolean hasPlayer(MultiplayerGame multiplayerGame, String userName) {
    PersonPlayer firstPlayer = multiplayerGame.getFirstPlayer();
    PersonPlayer secondPlayer = multiplayerGame.getSecondPlayer(); 

    if (firstPlayer != null) {
      String firstPlayerUserName = firstPlayer.getUsername();

      if (firstPlayerUserName.equals(userName))
        return true; 
    }

    if (secondPlayer != null) {
      String secondPlayerUserName = secondPlayer.getUsername();

      if (secondPlayerUserName.equals(userName))
        return true; 
    }

    return false;
  }

  public int findPlayerSymbol(MultiplayerGame multiplayerGame, String userName) {
    PersonPlayer firstPlayer = multiplayerGame.getFirstPlayer();
    PersonPlayer secondPlayer = multiplayerGame.getSecondPlayer(); 

    if (firstPlayer != null) {
      String firstPlayerUserName = firstPlayer.getUsername();

      if (firstPlayerUserName.equals(userName))
        return firstPlayer.getSymbol(); 
    }

    if (secondPlayer != null) {
      String secondPlayerUserName = secondPlayer.getUsername();

      if (secondPlayerUserName.equals(userName))
        return secondPlayer.getSymbol(); 
    }

    return 0;
  }

  public void addPlayer(MultiplayerGame multiplayerGame, PersonPlayer personPlayer) {
    if (multiplayerGame.getFirstPlayer() == null) {
      multiplayerGameRepository.updateFirstPlayer(multiplayerGame.getId(), personPlayer.getUsername(), 1);
    } else if (multiplayerGame.getSecondPlayer() == null) {
      multiplayerGameRepository.updateSecondPlayer(multiplayerGame.getId(), personPlayer.getUsername(), -1);
    }
  }

  public int getSymbolForPlayer(MultiplayerGame multiplayerGame, String userName) {
    PersonPlayer firstPlayer = multiplayerGame.getFirstPlayer();
    PersonPlayer secondPlayer = multiplayerGame.getSecondPlayer(); 

    if (firstPlayer.getUsername().equals(userName))
      return firstPlayer.getSymbol();
    else if (secondPlayer.getUsername().equals(userName))
      return secondPlayer.getSymbol();

    return 0;
  }

  public void makeAction(MultiplayerGame multiplayerGame, int action, int symbolForPlayer) {
    Board board = multiplayerGame.getBoard();
    board.setCell(action, symbolForPlayer);
    multiplayerGameRepository.updateBoard(multiplayerGame.getId(), board);
  }
}
