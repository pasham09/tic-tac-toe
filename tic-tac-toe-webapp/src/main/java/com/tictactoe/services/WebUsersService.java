package com.tictactoe.services;

import java.util.HashMap;
import java.util.Map;

import com.tictactoe.feign.UserClient;
import com.tictactoe.models.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class WebUsersService {

  @Autowired
  private UserClient userClient;

  protected String serviceUrl = "http://usermicroservice";

  public User findByUserName(String userName) {
    User user = userClient.findByUserName(userName);
    return user;
  }

  public Iterable<User> findAll() {
    Iterable<User> users = userClient.findAll();
    return users;
  }

  public HttpStatus save(User user) {
    return userClient.save(user);
  }

  public void incrementWins(String userName) {
    userClient.incrementWins(userName);
  }

  public void incrementLoses(String userName) {
    userClient.incrementLoses(userName);
  }
  
  public void editUser(String userName, String updatedUserName, String updatedRoles) {
    Map<String, String> map = new HashMap<>();
    map.put("userName", userName);
    map.put("updatedUserName", updatedUserName);
    map.put("updatedRoles", updatedRoles);

    userClient.editUser(map);
  }

}
