package com.tictactoe.services;

import com.tictactoe.models.Game;
import com.tictactoe.models.Player;
import com.tictactoe.utils.Board;
import com.tictactoe.utils.GameResult;

public class GameService {
  /**
   * Returns the game result.
   * 
   * @return Result of the game.
   */
  public GameResult getResult(Game<?> game) {
    GameResult gameResult = new GameResult();

    Board board = game.getBoard();
    Player firstPlayer = game.getFirstPlayer();
    Player secondPlayer = game.getSecondPlayer();

    for (int i = 0; i < 8; i++) {
      int sum = 0;
      switch (i) {
      case 0:
        sum = board.getCell(0) + board.getCell(1) + board.getCell(2);
        break;
      case 1:
        sum = board.getCell(3) + board.getCell(4) + board.getCell(5);
        break;
      case 2:
        sum = board.getCell(6) + board.getCell(7) + board.getCell(8);
        break;
      case 3:
        sum = board.getCell(0) + board.getCell(3) + board.getCell(6);
        break;
      case 4:
        sum = board.getCell(1) + board.getCell(4) + board.getCell(7);
        break;
      case 5:
        sum = board.getCell(2) + board.getCell(5) + board.getCell(8);
        break;
      case 6:
        sum = board.getCell(0) + board.getCell(4) + board.getCell(8);
        break;
      case 7:
        sum = board.getCell(2) + board.getCell(4) + board.getCell(6);
        break;
      }

      if (sum == 3) {
        gameResult.setWinner(firstPlayer);
        gameResult.setLoser(secondPlayer);

        return gameResult;
      } else if (sum == -3) {
        gameResult.setWinner(secondPlayer);
        gameResult.setLoser(firstPlayer);

        return gameResult;
      }
    }

    if (board.getAvailablePositions().size() == 0) {
      gameResult.setDraw();

      return gameResult;
    }

    return null;
  }
}
