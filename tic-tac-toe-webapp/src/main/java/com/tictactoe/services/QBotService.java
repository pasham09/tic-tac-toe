package com.tictactoe.services;

import java.util.Optional;

import com.tictactoe.models.QBot;
import com.tictactoe.repos.QBotRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QBotService {

  @Autowired
  private QBotRepository qBotRepository;

  public void save(QBot qBot) {
    qBotRepository.save(qBot);
  } 

  public QBot findFirstBySymbol(int symbol) {
    Optional<QBot> result = qBotRepository.findFirstBySymbol(symbol);
    if (!result.isPresent())
      return null;

    return result.get();
  }
}
