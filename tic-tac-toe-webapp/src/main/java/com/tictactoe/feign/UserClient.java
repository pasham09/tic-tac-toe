package com.tictactoe.feign;

import java.util.Map;

import com.tictactoe.models.User;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("usermicroservice")
public interface UserClient {
  @GetMapping("/user/all")
  Iterable<User> findAll();

  @GetMapping("/user/{userName}")
  User findByUserName(@PathVariable String userName);

  @PostMapping("/user/save")
  HttpStatus save(@RequestBody User user);

  @PostMapping("/user/wins/increment")
  void incrementWins(@RequestBody String userName);

  @PostMapping("/user/loses/increment")
  void incrementLoses(@RequestBody String userName);

  @PostMapping("/user/edit")
  void editUser(@RequestBody Map<String, String> map);
}
