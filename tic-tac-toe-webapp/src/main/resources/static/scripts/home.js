import {
  handlePlayerSymbolClick,
  handleCellClick as handleCellClickAiMode,
  resetGame,
} from "./aimode.js";

import {
  handleCellClick as handleCellClickMultiplayerMode,
  handleCreateButtonClick,
  displayGames,
} from "./multiplayermode.js";

import { clearBoard } from "./util.js";

let mode = 0; // 0 for ai, 1 for multiplayer

const handleAiModeClick = () => {
  mode = 0;
  clearBoard();

  const boardEl = document.querySelector(".board");
  const symbolEl = document.getElementById("player-symbol");
  const listGamesEl = document.querySelector(".list-games");

  boardEl.style.display = "block";
  symbolEl.style.display = "block";
  listGamesEl.style.display = "none";
};

const handleMultiplayerModeClick = async () => {
  mode = 1;
  displayGames();
};

const handleResetButtonClick = () => {
  document.querySelector(".overlay").style.display = "none";
  if (mode === 0) resetGame();
  else displayGames();
};

const handleCellClick = (cell, index) => {
  if (mode === 0) handleCellClickAiMode(cell, index);
  else handleCellClickMultiplayerMode(index);
};

const aiModeElem = document.querySelector(".ai-mode");
aiModeElem.addEventListener("click", handleAiModeClick);

const multiplayerModeElem = document.querySelector(".multiplayer-mode");
multiplayerModeElem.addEventListener("click", handleMultiplayerModeClick);

const playerSymbolEl = document.getElementById("player-symbol");
playerSymbolEl.addEventListener("click", () => {
  if (mode === 0) handlePlayerSymbolClick();
});

const buttonReset = document.getElementById("reset");
buttonReset.addEventListener("click", handleResetButtonClick);

const createButtonEl = document.getElementById("create");
createButtonEl.addEventListener("click", handleCreateButtonClick);

const cells = document.querySelectorAll(".cell");
cells.forEach((cell, index) => {
  cell.addEventListener("click", () => handleCellClick(cell, index));
});
