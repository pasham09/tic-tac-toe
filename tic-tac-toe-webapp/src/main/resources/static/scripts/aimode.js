import {
  clearBoard,
  displayDraw,
  displayWinner,
  displaySymbol,
  updateCellWithO,
  updateCellWithX,
  isWin,
  hasAvailablePositions,
} from "./util.js";

let playerSymbol = 1;
let currentPlayer = 1;
let board = [0, 0, 0, 0, 0, 0, 0, 0, 0];

/**
 * Resets the game, so player could start new one.
 */
const resetGame = async () => {
  board = [0, 0, 0, 0, 0, 0, 0, 0, 0];

  currentPlayer = 1;
  clearBoard();

  if (playerSymbol === -1) {
    const nextMove = await fetchNextMove();
    board[nextMove] = currentPlayer;
    const nextCell = document.querySelectorAll(".cell")[nextMove];

    updateCellWithX(nextCell);
    currentPlayer = -currentPlayer;
  }
};

/**
 * Handles players action to change symbol to play.
 * Resets the game and updates UI with new symbol.
 */
const handlePlayerSymbolClick = () => {
  playerSymbol = -playerSymbol;
  resetGame();
  displaySymbol(playerSymbol);
};

/**
 * Handles players action to make move.
 * Updates UI with players symbol and finds next move.
 * @param cell
 * @param index Position of the cell in board array.
 */
const handleCellClick = async (cell, index) => {
  if (cell.hasChildNodes()) return;

  move(cell, index);
  if (isWin(board)) {
    displayWinner(currentPlayer);

    if (currentPlayer === playerSymbol) {
      fetch("/user/updatewins", { method: "POST" });
    } else {
      fetch("/user/updateloses", { method: "POST" });
    }

    return;
  } else if (!hasAvailablePositions(board)) {
    displayDraw();
    return;
  }

  currentPlayer = -currentPlayer;

  const nextMove = await fetchNextMove();
  const nextCell = document.querySelectorAll(".cell")[nextMove];

  move(nextCell, nextMove);
  if (isWin(board)) {
    displayWinner(currentPlayer);

    if (currentPlayer === playerSymbol) {
      fetch("/user/updatewins", { method: "POST" });
    } else {
      fetch("/user/updateloses", { method: "POST" });
    }

    return;
  } else if (!hasAvailablePositions(board)) {
    displayDraw();
    return;
  }

  currentPlayer = -currentPlayer;
};

const move = (cell, index) => {
  board[index] = currentPlayer;

  if (currentPlayer === 1) {
    updateCellWithX(cell);
  } else if (currentPlayer === -1) {
    updateCellWithO(cell);
  }
};

/**
 * Fetches next move from back-end.
 * @returns {Integer} Position on the board to move to.
 */
const fetchNextMove = async () => {
  const response = await fetch(`/bot/play/${-playerSymbol}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(board),
  });

  const nextMove = await response.json();
  return nextMove;
};

export { handlePlayerSymbolClick, handleCellClick, resetGame };
