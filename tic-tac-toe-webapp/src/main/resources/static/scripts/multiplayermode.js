import {
  displayDraw,
  displaySymbol,
  displayWinner,
  isWin,
  hasAvailablePositions,
  clearBoard,
  updateCellWithX,
  updateCellWithO
} from "./util.js";

let stompClient = null;
let currentGameIdx;

const handleCreateButtonClick = async () => {
  await fetch("/game/create", { method: "POST" });
  displayGames();
};

const displayGames = async () => {
  const boardEl = document.querySelector(".board");
  const symbolEl = document.getElementById("player-symbol");
  const listGamesEl = document.querySelector(".list-games");

  boardEl.style.display = "none";
  symbolEl.style.display = "none";
  listGamesEl.style.display = "block";

  const response = await fetch("/game/all");
  const games = await response.json();
  const listContainerEl = document.querySelector(".list-container");
  listContainerEl.innerHTML = "";

  games.forEach((game) => {
    const gameEl = document.createElement("div");
    const firstPlayerUserName = game.firstPlayerUserName
      ? game.firstPlayerUserName
      : "[available]";
    const secondPlayerUserName = game.secondPlayerUserName
      ? game.secondPlayerUserName
      : "[available]";

    gameEl.innerHTML = `${firstPlayerUserName} against ${secondPlayerUserName}`;
    gameEl.classList.add("game");
    gameEl.addEventListener("click", () => handleGameClick(game.id));

    listContainerEl.appendChild(gameEl);
  });
};

const handleGameClick = async (gameIdx) => {
  currentGameIdx = gameIdx;
  await fetch(`/game/join/${gameIdx}`, { method: "POST" });

  const responseSymbol = await fetch(`/game/symbol/${gameIdx}`);
  const playerSymbol = await responseSymbol.json();

  if (playerSymbol === 0) return;

  clearBoard();

  const boardElem = document.querySelector(".board");
  const symbolElem = document.getElementById("player-symbol");
  const listGamesEl = document.querySelector(".list-games");

  boardElem.style.display = "block";
  symbolElem.style.display = "block";
  listGamesEl.style.display = "none";

  displaySymbol(playerSymbol);

  let socket = new SockJS("/gs-guide-websocket");
  stompClient = Stomp.over(socket);
  stompClient.connect({}, function () {
    stompClient.subscribe(`/topic/game/${gameIdx}`, function (board) {
      updateBoard(JSON.parse(board.body));
    });
  });

  const response = await fetch(`/game/${gameIdx}/board`);
  const board = await response.json();
  updateBoard(board);
};

async function updateBoard(board) {
  const cells = document.querySelectorAll(".cell");
  cells.forEach((cell, index) => {
    cell.innerHTML = "";

    if (board[index] === 1) {
      updateCellWithX(cell);
    } else if (board[index] === -1) {
      updateCellWithO(cell);
    }
  });

  if (isWin(board)) {
    displayWinner(1);
    stompClient.disconnect();
    return;
  } else if (!hasAvailablePositions(board)) {
    displayDraw();
    stompClient.disconnect();
    return;
  }
}

const handleCellClick = (action) => {
  stompClient.send(`/app/${currentGameIdx}/action`, {}, JSON.stringify(action));
};

export { handleCellClick, handleGameClick, handleCreateButtonClick, displayGames };
