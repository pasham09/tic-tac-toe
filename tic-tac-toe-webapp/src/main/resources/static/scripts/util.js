const clearBoard = () => {
  const cells = document.querySelectorAll(".cell");

  cells.forEach((cell) => {
    cell.innerHTML = "";
  });
};

const displayWinner = (currentPlayer) => {
  document.querySelector(".overlay-message").textContent = `${
    currentPlayer === 1 ? "X" : "O"
  } has won!`;
  document.querySelector(".overlay").style.display = "block";
};

const displayDraw = () => {
  document.querySelector(".overlay-message").textContent = `Draw!`;
  document.querySelector(".overlay").style.display = "block";
};

const displaySymbol = (symbol) => {
  const playerSymbolEl = document.getElementById("player-symbol");

  if (symbol === 1) {
    playerSymbolEl.classList.remove("o-symbol");
    playerSymbolEl.classList.add("x-symbol");
  } else if (symbol === -1) {
    playerSymbolEl.classList.remove("x-symbol");
    playerSymbolEl.classList.add("o-symbol");
  }
};

/**
 * Updates specified cell with X symbol.
 * @param cell DOM Element, div that represents cell.
 */
const updateCellWithX = (cell) => {
  const divX = document.createElement("div");
  divX.classList.add("x-symbol");
  cell.appendChild(divX);
};

/**
 * Updates specified cell with O symbol.
 * @param cell DOM Element, div that represents cell.
 */
const updateCellWithO = (cell) => {
  const divO = document.createElement("div");
  divO.classList.add("o-symbol");
  cell.appendChild(divO);
};

const winningConditions = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

/**
 * Checks if there is win condition on the board.
 * @param board Arrays of ints that represents board.
 * @returns true if there is a win on the board or false.
 */
const isWin = (board) => {
  for (let i = 0; i <= 7; i++) {
    const winCondition = winningConditions[i];

    let a = board[winCondition[0]];
    let b = board[winCondition[1]];
    let c = board[winCondition[2]];

    if (a === 0 || b === 0 || c === 0) {
      continue;
    }

    if (a === b && b === c) {
      return true;
    }
  }
  return false;
};

/**
 * Checks if board has available positions.
 * @param board Arrays of ints that represents board.
 * @returns {boolean} false if all cells are used, otherwise true.
 */
const hasAvailablePositions = (board) => {
  for (let i = 0; i < board.length; i++) {
    if (board[i] === 0) return true;
  }

  return false;
};

export {
  clearBoard,
  displayDraw,
  displayWinner,
  displaySymbol,
  updateCellWithO,
  updateCellWithX,
  isWin,
  hasAvailablePositions,
};
