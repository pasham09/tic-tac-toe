package com.usermicroservice.controllers;

import java.util.Map;

import com.usermicroservice.models.User;
import com.usermicroservice.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

  @Autowired
  UserService userService;

  @GetMapping("{userName}")
  public User findByUserName(@PathVariable String userName) {
    User user = userService.findByUserName(userName);
    return user;
  }

  @GetMapping("all")
  public Iterable<User> findAll() {
    return userService.findAll();
  }

  @PostMapping("save")
  public HttpStatus save(@RequestBody User user) {
    if (userService.findByUserName(user.getUserName()) != null)
      return HttpStatus.INTERNAL_SERVER_ERROR;

    userService.save(user);
    return HttpStatus.OK;
  }

  @PostMapping("edit")
  public void edit(@RequestBody Map<String, String> map) {
    String userName = map.get("userName");
    String updatedUserName = map.get("updatedUserName");
    String updatedRoles = map.get("updatedRoles");

    userService.updateUser(userName, updatedUserName, updatedRoles);
  }

  @PostMapping("wins/increment")
  public void incrementWins(@RequestBody String userName) {
    User user = userService.findByUserName(userName);
    userService.updateWins(userName, user.getWinsCount() + 1);
  }

  @PostMapping("loses/increment")
  public void incrementLoses(@RequestBody String userName) {
    User user = userService.findByUserName(userName);
    userService.updateLoses(userName, user.getLosesCount() + 1);
  }
}
