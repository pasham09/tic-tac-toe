package com.usermicroservice.services;

import java.util.Optional;

import com.usermicroservice.models.User;
import com.usermicroservice.repos.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
  @Autowired
  private UserRepository userRepository;
 
  public void save(User user) {
    user.setActive(true);
    user.setRoles("ROLE_USER,ROLE_ADMIN");
    userRepository.save(user);
  }

  public User findByUserName(String userName) {
    Optional<User> user = userRepository.findByUserName(userName);
    if (!user.isPresent()) {
      return null;
    }

    return user.get();
  }

  public void updateUser(String userName, String updatedUserName, String roles) {
    userRepository.updateUser(userName, updatedUserName, roles);
  }
  
  public void updateWins(String userName, int winsCount) {
    userRepository.updateWins(userName, winsCount);
  }

  public void updateLoses(String userName, int losesCount) {
    userRepository.updateLoses(userName, losesCount);
  }

  public Iterable<User> findAll() {
    return userRepository.findAll();
  }
}